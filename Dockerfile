FROM centos:latest

LABEL maintainer="Joerg Klein <kwp.klein@gmail.com>" \
      description="Docker base image for Sympa, a mailing list server"

RUN dnf update -y  \
    && dnf group install -y "Development Tools" \
    && dnf clean all \
    && rm -rf /var/cache/dnf

# Set locale
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

# Download the source file
ENV SYMPA_VERSION 6.2.48
ENV SYMPA_BINARY ${SYMPA_VERSION}.tar.gz

RUN curl -LJO https://github.com/sympa-community/sympa/archive/${SYMPA_BINARY} \
    && tar xzf sympa-${SYMPA_BINARY} -C /usr/src/ \
    && rm sympa-${SYMPA_BINARY}

RUN groupadd sympa \
    && useradd -g sympa -c 'Sympa' -s /sbin/nologin sympa

WORKDIR /usr/src/sympa-${SYMPA_VERSION}

# Install Sympa
RUN autoreconf -i \
    && automake --add-missing \
    && ./configure --enable-fhs --without-initdir --with-unitsdir=/etc/systemd/system \
    # && ./configure --enable-fhs --without-initdir --with-unitsdir=/etc/systemd/system --prefix=/usr/local --with-confdir=/etc/sympa \
    && make \
    && make install \
    && make clean

CMD ["/bin/bash"]
